import 'dart:io';

abstract class Drink {
  String name;
  String sweet;
  String value;
  Drink(this.name, this.sweet, this.value);
  void showDrinkResult();
  String getPrice();
}

class Recommend extends Drink {
  Recommend(String name, String sweet, String value)
      : super(name, sweet, value);

  @override
  void showDrinkResult() {
    print('__________________________________');
    print('Your drink is $name $sweet');
    print('Price $value baht');
  }

  @override
  String getPrice() {
    return value;
  }
}

class Coffee extends Drink {
  Coffee(String name, String sweet, String value) : super(name, sweet, value);

  @override
  void showDrinkResult() {
    print('__________________________________');
    print('Your drink is $name $sweet');
    print('Price $value baht');
  }

  @override
  String getPrice() {
    return value;
  }
}

class Tea extends Drink {
  Tea(String name, String sweet, String value) : super(name, sweet, value);

  @override
  void showDrinkResult() {
    print('__________________________________');
    print('Your drink is $name $sweet');
    print('Price $value baht');
  }

  @override
  String getPrice() {
    return value;
  }
}

class Milk extends Drink {
  Milk(String name, String sweet, String value) : super(name, sweet, value);

  @override
  void showDrinkResult() {
    print('__________________________________');
    print('Your drink is $name $sweet');
    print('Price $value baht');
  }

  @override
  String getPrice() {
    return value;
  }
}

class Protein extends Drink {
  Protein(String name, String sweet, String value) : super(name, sweet, value);

  @override
  void showDrinkResult() {
    print('__________________________________');
    print('Your drink is $name $sweet');
    print('Price $value baht');
  }

  @override
  String getPrice() {
    return value;
  }
}

class Soda extends Drink {
  Soda(String name, String sweet, String value) : super(name, sweet, value);

  @override
  void showDrinkResult() {
    print('__________________________________');
    print('Your drink is $name $sweet');
    print('Price $value baht');
  }

  @override
  String getPrice() {
    return value;
  }
}

void main(List<String> arguments) {
  String name = ' ';
  String sweet = ' ';
  String value = ' ';
  showTypeOfDrinkText();
  String? type = stdin.readLineSync();
  selectMenuProcess(type, name, value, sweet);
  selectPaymentProcess();
  print('Take your drink, thank you');
}

void selectPaymentProcess() {
  print('-Payment-');
  print('1.Cash\n2.Scan QR');
  print('Select number of Payment: ');
  String? payment = stdin.readLineSync();
  print('__________________________________');
  if (payment == '1') {
    print('paid by Cash');
  } else if (payment == '2') {
    print('paid by Scan QR');
  }
  print('__________________________________');
}

void showTypeOfDrinkText() {
  print('-Type of drink-');
  print(
      '1.Recommend\n2.Coffee\n3.Tea\n4.Milk Cocoa and Caramel\n5.Protein Shake\n6.Soda and Others');
  print('Select number of the type of drink: ');
}

void selectMenuProcess(String? type, String name, String value, String sweet) {
  if (type == '1') {
    print('__________________________________');
    print('-Menu Recommend-');
    print('1.Iced Tea 40 Baht\n2.Iced Latte 40 Baht\n3.Hot Espresso 35 Baht');
    print('Select number of drink menu: ');
    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'Iced Tea';
      value = '40';
    } else if (name == '2') {
      name = 'Iced Latte';
      value = '40';
    } else if (name == '3') {
      name = 'Hot Espresso';
      value = '35';
    }

    showSweetLevelText();
    sweet = stdin.readLineSync()!;
    sweet = sweetLevel(sweet);

    Drink drink = Recommend(name, sweet, value);
    drink.showDrinkResult();
  } else if (type == '2') {
    print('__________________________________');
    print('-Coffee Menu-');
    print('1.Iced Mocha 40 Baht\n2.Iced Latte 40 Baht\n3.Hot Espresso 35 Baht');
    print('Select number of drink menu: ');
    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'Iced Mocha';
      value = '40';
    } else if (name == '2') {
      name = 'Iced Latte';
      value = '40';
    } else if (name == '3') {
      name = 'Hot Espresso';
      value = '35';
    }

    showSweetLevelText();
    sweet = stdin.readLineSync()!;
    sweet = sweetLevel(sweet);

    Drink drink = Coffee(name, sweet, value);
    drink.showDrinkResult();
  } else if (type == '3') {
    print('__________________________________');
    print('-Tea Menu-');
    print('1.Iced Tea 40 Baht\n2.Iced Tea Lemon 40 Baht\n3.Hot Tea 35 Baht');
    print('Select number of drink menu: ');
    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'Iced Tea';
      value = '40';
    } else if (name == '2') {
      name = 'Iced Tea Lemon';
      value = '40';
    } else if (name == '3') {
      name = 'Hot Tea';
      value = '35';
    }

    showSweetLevelText();
    sweet = stdin.readLineSync()!;
    sweet = sweetLevel(sweet);

    Drink drink = Tea(name, sweet, value);
    drink.showDrinkResult();
  } else if (type == '4') {
    print('__________________________________');
    print('-Milk Cocoa and Caramel Menu-');
    print('1.Iced Cocoa 40 Baht\n2.Hot Cocoa 35 Baht\n3.Iced Milk 40 Baht');
    print('Select number of drink menu: ');
    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'Iced Cocoa';
      value = '40';
    } else if (name == '2') {
      name = 'Hot Cocoa';
      value = '35';
    } else if (name == '3') {
      name = 'Iced Milk';
      value = '40';
    }

    showSweetLevelText();
    sweet = stdin.readLineSync()!;
    sweet = sweetLevel(sweet);

    Drink drink = Milk(name, sweet, value);
    drink.showDrinkResult();
  } else if (type == '5') {
    print('__________________________________');
    print('-Protein Shake Menu-');
    print(
        '1.Matcha Protein Shake 65 Baht\n2.Chocolate Protein Shake 65 Baht\n3.Thai Tea Protein Shake 65 Baht');
    print('Select number of drink menu: ');
    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'Matcha Protein Shake';
      value = '65';
    } else if (name == '2') {
      name = 'Chocolate Protein Shake';
      value = '65';
    } else if (name == '3') {
      name = 'Thai Tea Protein Shake';
      value = '65';
    }

    showSweetLevelText();
    sweet = stdin.readLineSync()!;
    sweet = sweetLevel(sweet);

    Drink drink = Protein(name, sweet, value);
    drink.showDrinkResult();
  } else if (type == '6') {
    print('__________________________________');
    print('-Soda and Others Menu-');
    print(
        '1.Pepsi 25 Baht\n2.Iced Lemon Soda 30 Baht\n3.Iced Sala Soda 30 Baht');
    print('Select number of drink menu: ');
    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'Pepsi';
      value = '25';
    } else if (name == '2') {
      name = 'Iced Lemon Soda';
      value = '30';
    } else if (name == '3') {
      name = 'Iced Sala Soda';
      value = '30';
    }

    showSweetLevelText();
    sweet = stdin.readLineSync()!;
    sweet = sweetLevel(sweet);

    Drink drink = Soda(name, sweet, value);
    drink.showDrinkResult();
  }
}

void showSweetLevelText() {
  print('__________________________________');
  print('-Sweet Level-');
  print('1.No sugar\n2.Sweetless\n3.Normal\n4.Very sweet\n5.Very sweet x3');
  print('Select number of sweet level: ');
}

String sweetLevel(String sweet) {
  if (sweet == '1') {
    sweet = 'No sugar';
  } else if (sweet == '2') {
    sweet = 'Sweetless';
  } else if (sweet == '3') {
    sweet = 'Normal';
  } else if (sweet == '4') {
    sweet = 'Very sweet';
  } else if (sweet == '5') {
    sweet = 'Very sweet x3';
  }
  return sweet;
}
